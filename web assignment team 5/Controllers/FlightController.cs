﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web_assignment_team_5.DAL;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.Controllers
{
    public class FlightController : Controller
    {

        private FlightScheduleDAL flightScheduleContext = new FlightScheduleDAL();
        private FlightRouteDAL flightRouteContext = new FlightRouteDAL();
        private List<SelectListItem> ArrivalFlightCountry = new List<SelectListItem>();
        private List<SelectListItem> DepartureFlightCountry = new List<SelectListItem>();
        private List<SelectListItem> ArrivalFlightCity = new List<SelectListItem>();
        private List<SelectListItem> DepartureFlightCity = new List<SelectListItem>();
        private FlightScheduleDAL flightscheduleContext = new FlightScheduleDAL();
        private AircraftDAL aircraftContext = new AircraftDAL();
        FlightRouteDAL flightrouteContext = new FlightRouteDAL();

        public IActionResult Index()
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<FlightSchedule> flightList = flightScheduleContext.GetAllFlightSchedule();
            return View(flightList);
        }


        //GET All flightroute
        //Get all flight scedhule and assign to each route
        public ActionResult Viewflights()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<FlightRoute> flightList = flightrouteContext.GetAllFlightRoutes();

            for (int i = 0; i < flightList.Count; i++)
            {
                flightList[i].flightSchedules = flightscheduleContext.GetRouteIDFlightSchedule(flightList[i].RouteID);
            }
            return View(flightList);
        }

       //get a drop down list containing routeid
       private List<SelectListItem> GetRouteID()
        {
            List<FlightRoute> routeList = flightrouteContext.GetAllFlightRoutes();
            List<SelectListItem> routeIDList = new List<SelectListItem>();
            foreach(FlightRoute item in routeList)
            {
                routeIDList.Add(new SelectListItem {Value = item.RouteID.ToString(),Text =item.RouteID.ToString() } );
            }
            return routeIDList;
        }
        //to update the status
        public ActionResult Update(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided 
                //Return to listing page, not allowed to edit 
                return RedirectToAction("Index");
            }
            FlightSchedule flight = flightscheduleContext.GetDetails(id.Value);
            if (flight == null)
            { //Return to listing page, not allowed to edit 
                return RedirectToAction("Index");
            }
            return View(flight);
        }
        //posting the changes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(FlightSchedule flight)
        {
            if (ModelState.IsValid)
            {
                flightscheduleContext.Update(flight);
                return RedirectToAction("Viewflights");
            }
            else
            {
                //validation fail, return to view
                return View(flight);
            }
        }

        // GET: FlightController/Create
        public ActionResult CreateRoute()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        // GET: FlightController/Create
        // POST: FlightController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRoute(FlightRoute flightRoute)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }


            int routeID = flightrouteContext.CreateRoute(flightRoute);
            if (routeID != 0)
            {
                ModelState.AddModelError("Route", "Successfully created and assigned to " + routeID);
            }
            return View();
        }

        // GET: FlightController/Create
        // POST: FlightController/Create
        public ActionResult CreateSchedule()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewData["RouteIDList"] = GetRouteID();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSchedule(FlightSchedule flightSchedule)
        {

            ViewData["RouteIDList"] = GetRouteID();
            if (ModelState.IsValid)
            {
                flightSchedule.ScheduleID=flightscheduleContext.CreateSchedule(flightSchedule);
                return RedirectToAction("Viewflights");
            }
            else
            {
                //validation fail, return to view
                return View(flightSchedule);
            }
        }



        //Get: FlightController/details/5

        public IActionResult FlightSchedule(int id)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("ViewSchedule");
            }
            FlightSchedule f = flightScheduleContext.GetDetails(id);
            FlightScheduleViewModel flightVM = MapToflightVM(f);
            return View(flightVM);
        }
        
        public FlightScheduleViewModel MapToflightVM(FlightSchedule flight)
        {
            List<int> tempAircraftId = new List<int>();
            List<string> tempAircraftStatus = new List<string>();
            List<DateTime> tempDepartureDateTime = new List<DateTime>();
            List<DateTime> tempArrivalDateTime = new List<DateTime>();
            List<string> tempDepartureFlightCountry = new List<string>();
            List<string> tempDepartureFlightCity = new List<string>();
            List<string> tempArrivalFlightCountry = new List<string>();
            List<string> tempArrivalFlightcity = new List<string>();

            if (flight.ScheduleID != 1)
            {
                List<Aircraft> aircraftList = aircraftContext.GetAllAircrafts();
                List<FlightRoute> flightRouteList = flightRouteContext.GetAllFlightRoutes();
                foreach (Aircraft aircraft in aircraftList)
                {
                    if (flight.AircraftID == aircraft.AircraftID)
                    {
                        tempAircraftId.Add(aircraft.AircraftID);
                        tempAircraftStatus.Add(aircraft.Status);
                        tempDepartureDateTime.Add(flight.DepartureDateTime);
                        tempArrivalDateTime.Add(flight.ArrivalDateTime);

                        foreach (FlightRoute f in flightRouteList)
                        {
                            if (f.RouteID == flight.RouteID)
                            {
                                tempDepartureFlightCity.Add(f.DepartureFlightCity);
                                tempDepartureFlightCountry.Add(f.DepartureFlightCountry);
                                tempArrivalFlightcity.Add(f.DepartureFlightCity);
                                tempArrivalFlightCountry.Add(f.ArrivalFlightCountry);
                                break;
                            }
                        }
                    }
                }

            }
            FlightScheduleViewModel flightVM = new FlightScheduleViewModel
            {
                ScheduleID = flight.ScheduleID,
                //RouteID = flight.RouteID,
                FlightNumber = flight.FlightNumber/*
                AircraftID = tempAircraftId,
                DepartureFlightCity = tempDepartureFlightCity,
                DepartureFlightCountry = tempDepartureFlightCountry,
                ArrivalFlightCity = tempArrivalFlightcity,
                ArrivalFlightCountry = tempArrivalCountry,
                DepartureDateTime = tempDepartureDateTime,
                ArrivalDateTime = tempArrivalDateTime*/

            };
            return flightVM;
            
        }

    }
}
