﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web_assignment_team_5.DAL;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.Controllers
{
    public class AircraftController : Controller
    {
        private AircraftDAL aircraftContext = new AircraftDAL();
        private FlightScheduleDAL flightScheduleContext = new FlightScheduleDAL();
        private FlightRouteDAL flightRouteContext = new FlightRouteDAL();

        // GET: Aircraft
        public IActionResult ViewAircrafts()
        {
            if((HttpContext.Session.GetString("Role") == null) || 
               (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("ViewAircrafts");
            }

            List<Aircraft> aircraftList = aircraftContext.GetAllAircrafts();
            return View(aircraftList);
        }

        // GET: Aircraft/Details/5
        public IActionResult AircraftSchedule(int id)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("ViewAircrafts");
            }
            Aircraft aircraft = aircraftContext.GetDetails(id);
            AircraftViewModel aircraftVM = MapToAircraftVM(aircraft);
            return View(aircraftVM);
        }

        public AircraftViewModel MapToAircraftVM(Aircraft aircraft)
        {
            List<DateTime> tempDepartureDateTime = new List<DateTime>();
            List<DateTime> tempArrivalDateTime = new List<DateTime>();
            List<string> tempDepartureCountry = new List<string>();
            List<string> tempArrivalCountry = new List<string>();

            if(aircraft.AircraftID != 0)
            {
                List<FlightSchedule> flightScheduleList = flightScheduleContext.GetAllFlightSchedule();
                List<FlightRoute> flightRouteList = flightRouteContext.GetAllFlightRoutes();
                foreach (FlightSchedule flightSched in flightScheduleList)
                {
                    if (flightSched.AircraftID == aircraft.AircraftID)
                    {
                        tempDepartureDateTime.Add(flightSched.DepartureDateTime);
                        tempArrivalDateTime.Add(flightSched.ArrivalDateTime);

                        foreach(FlightRoute f in flightRouteList)
                        {
                            if (f.RouteID == flightSched.RouteID)
                            {
                                        
                                tempDepartureCountry.Add(f.DepartureFlightCountry);
                                tempArrivalCountry.Add(f.ArrivalFlightCountry);
                                break;
                            }
                        }
                    }
                }
            }

            AircraftViewModel aircraftVM = new AircraftViewModel
            {
                AircraftID = aircraft.AircraftID,
                MakeModel = aircraft.MakeModel,
                DepartureDateTime = tempDepartureDateTime,
                ArrivalDateTime = tempArrivalDateTime,
                DepartureCountry = tempDepartureCountry,
                ArrivalCountry = tempArrivalCountry

            };
            return aircraftVM;
        }

               
        // GET: Aircraft/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Aircraft/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Aircraft aircraft)
        {
            if (ModelState.IsValid)
            {
                aircraft.AircraftID = aircraftContext.Add(aircraft);
                return RedirectToAction("ViewAircrafts");
            }
            else
            {
                return View(aircraft);
            }
        }

        public IActionResult CreateAircraft()
        {
            return View();
        }

        public IActionResult UpdateAircraft()
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            
            List<Aircraft> acList = aircraftContext.GetAllAircrafts();
            List<FlightSchedule> fsList = flightScheduleContext.GetAllFlightSchedule();
            List<Aircraft> aircraftList = new List<Aircraft>();
            
            foreach(Aircraft ac in acList)
            {
                aircraftList.Add(ac);
                foreach(FlightSchedule fs in fsList)
                {
                    if((fs.AircraftID == ac.AircraftID) && (ac.Status == "Operational") && (fs.DepartureDateTime >= DateTime.Now) )
                    {
                        aircraftList.Remove(ac);
                    }
                }
            }
            
            return View(aircraftList);
            
        }

        public IActionResult UpdateAircraftFiltered()
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            
            DateTime thirtydays = DateTime.Now.AddDays(-30);
            List<Aircraft> aircraftFiltered = new List<Aircraft>();
            List<Aircraft> aircraftList = aircraftContext.GetAllAircrafts();
            List<FlightSchedule> fsList = flightScheduleContext.GetAllFlightSchedule();

            //Filters to only aircrafts with last maintenance more than 30 days ago and displays it after button is clicked
            foreach (Aircraft ac in aircraftList)
            {
                aircraftFiltered.Add(ac);
                foreach (FlightSchedule fs in fsList)
                {
                    if (ac.DateLastMaintenance > thirtydays || ((fs.AircraftID == ac.AircraftID) && (ac.Status == "Operational") && (fs.DepartureDateTime >= DateTime.Now)))
                    {
                        aircraftFiltered.Remove(ac);
                    }
                }
            }

            return View(aircraftFiltered);
        }

        // GET: Aircraft/Edit/5
        public ActionResult EditAircraft(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            //ViewData["BranchList"] = GetAllBranches();
            Aircraft aircraft = aircraftContext.GetDetails(id.Value);
            if (aircraft == null) {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(aircraft);
        }

        // POST: Aircraft/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAircraft(Aircraft aircraft)
        {
            if (ModelState.IsValid) {
            //Update staff record to database
                aircraftContext.Update(aircraft);
                return RedirectToAction("UpdateAircraft");
            }
            else {
            //Input validation fails, return to the view
            //to display error message

                return View(aircraft);
            }   
             
        }

        public IActionResult AssignSchedule()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<FlightSchedule> scheduleNotAssigned = new List<FlightSchedule>();
            List<Aircraft> acList = aircraftContext.GetAllAircrafts();
            List<FlightSchedule> fsList = flightScheduleContext.GetAllFlightSchedule();
            DateTime today = DateTime.Now;

            //Checks if aircraftID matches with schedule and if departuredate is before today and removes it from list
            foreach(FlightSchedule fs in fsList)
            {
                scheduleNotAssigned.Add(fs);
                foreach(Aircraft ac in acList)
                {
                    if ((fs.AircraftID == ac.AircraftID) || (fs.DepartureDateTime < today.Date))
                    {
                        scheduleNotAssigned.Remove(fs);
                    }
                }
            }

            return View(scheduleNotAssigned);
        }

        private List<Aircraft> GetAllAircrafts()
        {
            List<Aircraft> aircraftList = new List<Aircraft>();
            List<Aircraft> acList = aircraftContext.GetAllAircrafts();
            foreach(Aircraft ac in acList)
            {
                if (ac.Status == "Operational")
                {
                    aircraftList.Add(ac);
                }
            }

            aircraftList.Insert(0, new Aircraft
            {
                AircraftID = 0,
                MakeModel = "--Select--"
            });
            return aircraftList;
        }

        public ActionResult AssignAircraft(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            ViewData["AircraftList"] = GetAllAircrafts();
            FlightSchedule flightSchedule = flightScheduleContext.GetDetails(id.Value);
            if (flightSchedule == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(flightSchedule);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignAircraft(FlightSchedule flightSchedule)
        {
            if (ModelState.IsValid)
            {
                //Update staff record to database
                flightScheduleContext.UpdateAircraft(flightSchedule);
                return RedirectToAction("AssignSchedule");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message

                return View(flightSchedule);
            }

        }



    }
}