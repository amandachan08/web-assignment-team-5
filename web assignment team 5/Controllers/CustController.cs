﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using web_assignment_team_5.DAL;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.Controllers
{
    public class CustController : Controller
    {
        private CustomerDAL customerContext = new CustomerDAL();

        // GET: Cust
        public ActionResult Index()
        {
            return View();
        }

        // GET: Cust/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cust/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cust/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustReg customer)
        {
            if (ModelState.IsValid)
            {
                //Add Customer record to database
                customer.CustomerID = customerContext.Add(customer);
                //Redirect user to Staff/Index view
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(customer);
            }
        }

        // GET: Cust/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Cust/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustReg customer)
        {
            if (ModelState.IsValid)
            {
                //Update staff record to database
                customerContext.Update(customer);
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(customer);
            }
        }

        // GET: Cust/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cust/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}