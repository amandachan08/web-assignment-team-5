﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using web_assignment_team_5.Models;
using System.Net.Http;
using Newtonsoft.Json;
using web_assignment_team_5.DAL;

namespace web_assignment_team_5.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }


        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();
            CustomerDAL customerContext = new CustomerDAL();
            int? custID = customerContext.isUserExist(loginID, password);
            if (custID !=null)
                {
                HttpContext.Session.SetInt32("CustomerID", (int)custID);
                return RedirectToAction("CustomerMain");
                  // Store Login ID in session with the key “LoginID” 
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role” 
                HttpContext.Session.SetString("Role", "Customer");
            }
         
            else if (loginID == "s1234567@lca.com" && password == "p@55Staff") 
            {
                // Store Login ID in session with the key “LoginID” 
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role” 
                HttpContext.Session.SetString("Role", "Admin");
                // Redirect user to the "StaffMain" view through an action
                return RedirectToAction("StaffMain");
            }
            else
            {
                // Store an error message in TempData for display at the index view 
                TempData["Message"] = "Invalid Login Credentials!";
                // Redirect user back to the index view through an action
                return RedirectToAction("Login");
            }
        }
        public IActionResult CustomerMain()
        {
            return View();
        }
        public IActionResult StaffMain()
        {
            return View();
        }
        public ActionResult LogOut()
        { 
            // Clear all key-values pairs stored in session state 
            HttpContext.Session.Clear();
            // Call the Index action of Home controller 
            return RedirectToAction("Index"); 
        }
            public IActionResult Create()
        {

            return View();
        }
        public IActionResult About()
        {
            return View();
        }
        public IActionResult TravelInfo()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
        public async Task<ActionResult> Weather()
        {
            string currentDateTime = DateTime.Now.ToString("yyyy-MM-dd");
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.data.gov.sg");
            HttpResponseMessage response = await client.GetAsync("/v1/environment/24-hour-weather-forecast?date=" + currentDateTime);

            if(response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                var weather = JsonConvert.DeserializeObject<Weather>(data);
                return View(weather);
            }
            else
            {
                return View(null);
            }

        }



    }

}
