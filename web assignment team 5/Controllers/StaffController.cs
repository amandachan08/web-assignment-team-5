﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using web_assignment_team_5.DAL;
using web_assignment_team_5.Models;


namespace web_assignment_team_5.Controllers
{
    public class StaffController : Controller
    {
        private StaffDAL staffContext = new StaffDAL();
        private FlightCrewDAL flightcrewContext = new FlightCrewDAL();
        private FlightScheduleDAL flightscheduleContext = new FlightScheduleDAL();
        public IActionResult Index()
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<Staff> staffList = staffContext.GetAllStaff();
            return View(staffList);
        }

        // GET: Staff/Create
        public IActionResult Create()
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["VocationList"] = GetVocation();
            return View();

        }

        // POST: Staff/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Staff staff)
        {
            //Get vocation list for drop-down list
            //in case of the need to return to Create.cshtml view
            ViewData["VocationList"] = GetVocation();
            if (ModelState.IsValid)
            {
                //Add staff record to database
                staff.StaffID = staffContext.Add(staff);
                //Redirect user to Staff/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(staff);
            }
        }

        private List<SelectListItem> GetVocation()
        {
            List<SelectListItem> vocation = new List<SelectListItem>();
            vocation.Add(new SelectListItem { Value = "Administrator", Text = "Administrator" });
            vocation.Add(new SelectListItem { Value = "Pilot", Text = "Pilot" });
            vocation.Add(new SelectListItem { Value = "Flight Attendant", Text = "Flight Attendant" });

            return vocation;
        }

        // GET: Staff/Details/5
        public IActionResult Details(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            Staff staff = staffContext.GetDetails(id);
            StaffViewModel staffVM = MapToStaffVM(staff);
            return View(staffVM);
        }

        public StaffViewModel MapToStaffVM(Staff staff)
        {
            List<int> tempScheduleNo = new List<int>();
            List<string> tempRole = new List<string>();
            List<DateTime> tempDepartureDateTime = new List<DateTime>();
            if (staff.Vocation != "Administrator")
            {
                List<FlightCrew> flightcrewList = flightcrewContext.GetAllFlightCrew();
                List<FlightSchedule> flightscheduleList = flightscheduleContext.GetAllFlightSchedule();
                foreach (FlightCrew fc in flightcrewList)
                {
                    if (fc.StaffID == staff.StaffID)
                    {
                        tempRole.Add(fc.Role);
                        tempScheduleNo.Add(fc.ScheduleID);
                        foreach (FlightSchedule fs in flightscheduleList)
                        {
                            if (fc.ScheduleID == fs.ScheduleID)
                            {
                                tempDepartureDateTime.Add(fs.DepartureDateTime);
                                break;
                            }
                        }
                    }
                }

            }

            StaffViewModel staffVM = new StaffViewModel
            {
                StaffID = staff.StaffID,
                StaffName = staff.StaffName,
                Gender = staff.Gender,
                DateEmployed = staff.DateEmployed,
                Vocation = staff.Vocation,
                EmailAddr = staff.EmailAddr,
                Status = staff.Status,
                ScheduleNo = tempScheduleNo,
                Role = tempRole,
                DepartureDateTime = tempDepartureDateTime
            };

            return staffVM;
        }

        private List<DateTime> getDepartureDate (Staff s, List<FlightCrew> fcList, List<FlightSchedule> fsList) // get list of depareture date for each staff
        {
            List<DateTime> tempDepartureDateTime = new List<DateTime>();
            if (s.Vocation != "Administrator")
            {
                foreach (FlightCrew fc in fcList)
                {
                    if (fc.StaffID == s.StaffID)
                    {
                        foreach (FlightSchedule fs in fsList)
                        {
                            if (fc.ScheduleID == fs.ScheduleID)
                            {
                                DateTime tempDate = fs.DepartureDateTime.Date;
                                tempDepartureDateTime.Add(tempDate);
                                break;
                            }
                        }
                    }
                }

            }

            return tempDepartureDateTime;
        }

        public IActionResult UpdatePersonnel()
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<Staff> personnelList = new List<Staff>();
            List<Staff> staffList = staffContext.GetAllStaff();
            DateTime currentDate = DateTime.Now.Date;

            List<FlightCrew> flightcrewList = flightcrewContext.GetAllFlightCrew();
            List<FlightSchedule> flightscheduleList = flightscheduleContext.GetAllFlightSchedule();

            foreach (Staff s in staffList)
            {
                bool check = UpdateEligibility(s, currentDate, flightcrewList, flightscheduleList);
                bool assigned = checkAssigned(s, flightcrewList);
                if(check == true)
                {
                    personnelList.Add(s);
                }
                if(assigned == false)
                {
                    personnelList.Add(s);
                }
            }

            return View(personnelList);
        }

        private bool UpdateEligibility(Staff s, DateTime currentDate, List<FlightCrew> fcList, List<FlightSchedule> fsList)
        {
            bool check = false;
            List<DateTime> departureDateList = getDepartureDate(s, fcList, fsList);
            foreach(DateTime tempDate in departureDateList)
            {
                if (tempDate < currentDate)
                {
                    check = true;
                }
                else 
                {
                    check = false;
                    break;
                }
            }
            return check;
        }

        private bool checkAssigned(Staff s, List<FlightCrew> fcList)
        {
            bool check = true; // if its admin 
            if (s.Vocation != "Administrator")
            {
                check = false;
                foreach (FlightCrew fc in fcList)
                {
                    if (s.StaffID == fc.StaffID)
                    {
                        check = true;
                    }
                }
            }
            
            return check;
        }

        // GET: Staff/Edit
        public IActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided 
                //Return to listing page, not allowed to edit 
                return RedirectToAction("Index");
            }
            Staff staff = staffContext.GetDetails(id.Value);
            if (staff == null)
            { //Return to listing page, not allowed to edit 
                return RedirectToAction("Index");
            }
            return View(staff);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Staff staff)
        {
            if (ModelState.IsValid)
            {
                //Update staff record to database
                staffContext.Update(staff);
                return RedirectToAction("UpdatePersonnel");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(staff);
            }
        }

        public IActionResult AssignSchedule()
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<int> scheduledAssigned = new List<int>();
            List<FlightSchedule> fsList = new List<FlightSchedule>();

            List<FlightCrew> flightcrewList = flightcrewContext.GetAllFlightCrew();
            List<FlightSchedule> flightscheduleList = flightscheduleContext.GetAllFlightSchedule();

            scheduledAssigned = getAssignedID(flightcrewList);
            foreach (int id in scheduledAssigned)
            {
                checkNoOfRecords(id, flightcrewList);
            }
            scheduledAssigned = getAssignedID(flightcrewList);

            foreach (FlightSchedule fs in flightscheduleList)
            {
                bool check = scheduleExist(fs.ScheduleID, scheduledAssigned);
                if (check == false)
                {
                    fsList.Add(fs); // add unassigned 
                }
            }

            return View(fsList);
        }

        private List<int> getAssignedID (List<FlightCrew> fcList) // get list of schedule id that are assigned
        {
            List<int> assignedList = new List<int>();
            int currentID = 0;
            foreach (FlightCrew fc in fcList) // get list of schedule id that are assigned
            {
                if (fc.ScheduleID != currentID)
                {
                    assignedList.Add(fc.ScheduleID);
                }
                currentID = fc.ScheduleID;
            }

            return assignedList;
        }

        private bool scheduleExist(int fsID, List<int> saList) // check if schedule is in scheduleAssigned list
        {
            bool check = false;
            foreach (int id in saList)
            {
                if (id == fsID)
                {
                    check = true;
                    break;
                }
            }

            return check;
        }

        private void checkNoOfRecords(int id, List<FlightCrew> fcList) // check if there are 6 records for each schedule id in flightcrew table and delete if not
        {
            int count = 0;
            foreach (FlightCrew fc in fcList)
            {
                if(fc.ScheduleID == id)
                {
                    count += 1;
                }
            }

            if (count != 6)
            {
                flightcrewContext.Delete(id);
            }
        }

        private List<SelectListItem> GetFlightAttendant(int id) // get flight attendant list that are available
        {
            List<FlightCrew> flightcrewList = flightcrewContext.GetAllFlightCrew();
            List<FlightSchedule> flightscheduleList = flightscheduleContext.GetAllFlightSchedule();
            List<Staff> staffList = staffContext.GetAllStaff();
            DateTime scheduleDate = flightscheduleList[id - 1].DepartureDateTime.Date;

            List<SelectListItem> flightAttendant = new List<SelectListItem>();

            foreach (Staff s in staffList)
            {
                if (s.Vocation == "Flight Attendant" && s.Status == "Active")
                {
                    List<DateTime> tempDateList = getDepartureDate(s, flightcrewList, flightscheduleList);
                    bool check = sameDepartureDate(scheduleDate, tempDateList);
                    
                    if(check == false)
                    {
                        flightAttendant.Add(new SelectListItem { Value = s.StaffID.ToString(), Text = s.StaffID.ToString() + " - " + s.StaffName });
                    }
                }
            }

            return flightAttendant;
        }

        private List<SelectListItem> GetPilot(int id) // get pilot list that are available
        {
            List<FlightCrew> flightcrewList = flightcrewContext.GetAllFlightCrew();
            List<FlightSchedule> flightscheduleList = flightscheduleContext.GetAllFlightSchedule();
            List<Staff> staffList = staffContext.GetAllStaff();
            DateTime scheduleDate = flightscheduleList[id - 1].DepartureDateTime.Date;

            List<SelectListItem> pilot = new List<SelectListItem>();

            foreach (Staff s in staffList)
            {
                if (s.Vocation == "Pilot" && s.Status == "Active")
                {
                    List<DateTime> tempDateList = getDepartureDate(s, flightcrewList, flightscheduleList);
                    bool check = sameDepartureDate(scheduleDate, tempDateList);

                    if (check == false)
                    {
                        pilot.Add(new SelectListItem { Value = s.StaffID.ToString(), Text = s.StaffID.ToString() + " - " + s.StaffName });
                    }
                }
            }

            return pilot;
        }

        private bool sameDepartureDate(DateTime date, List<DateTime> dList) // check if it has the same departure date
        {
            bool check = false;
            foreach(DateTime d in dList)
            {
                if (date == d)
                {
                    check = true;
                }
            }

            return check;
        }

        public IActionResult FlightCaptain(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            FlightCrew fc = new FlightCrew
            {
                ScheduleID = id,
                Role = "Flight Captain"
            };
            ViewData["PilotList"] = GetPilot(id);

            return View(fc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FlightCaptain(FlightCrew fc)
        {
            ViewData["PilotList"] = GetPilot(fc.ScheduleID);
            if (ModelState.IsValid)
            {
                flightcrewContext.Add(fc);
                return RedirectToAction("SecondPilot", new { id = fc.ScheduleID });
            }
            else
            {
                return View(fc);
            }
        }

        public IActionResult SecondPilot(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            FlightCrew fc = new FlightCrew
            {
                ScheduleID = id,
                Role = "Second Pilot"
            };
            ViewData["PilotList"] = GetPilot(id);

            return View(fc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SecondPilot(FlightCrew fc)
        {
            ViewData["PilotList"] = GetPilot(fc.ScheduleID);
            if (ModelState.IsValid)
            {
                flightcrewContext.Add(fc);
                return RedirectToAction("CabinCrewLeader", new { id = fc.ScheduleID });
            }
            else
            {
                return View(fc);
            }
        }

        public IActionResult CabinCrewLeader(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            FlightCrew fc = new FlightCrew
            {
                ScheduleID = id,
                Role = "Cabin Crew Leader"
            };
            ViewData["FlightAttendantList"] = GetFlightAttendant(id);

            return View(fc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CabinCrewLeader(FlightCrew fc)
        {
            ViewData["FlightAttendantList"] = GetFlightAttendant(fc.ScheduleID);
            if (ModelState.IsValid)
            {
                flightcrewContext.Add(fc);
                return RedirectToAction("FlightAttendant_1", new { id = fc.ScheduleID });
            }
            else
            {
                return View(fc);
            }
        }

        public IActionResult FlightAttendant_1(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            FlightCrew fc = new FlightCrew
            {
                ScheduleID = id,
                Role = "Flight Attendant"
            };
            ViewData["FlightAttendantList"] = GetFlightAttendant(id);

            return View(fc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FlightAttendant_1(FlightCrew fc)
        {
            ViewData["FlightAttendantList"] = GetFlightAttendant(fc.ScheduleID);
            if (ModelState.IsValid)
            {
                flightcrewContext.Add(fc);
                return RedirectToAction("FlightAttendant_2", new { id = fc.ScheduleID });
            }
            else
            {
                return View(fc);
            }
        }

        public IActionResult FlightAttendant_2(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            FlightCrew fc = new FlightCrew
            {
                ScheduleID = id,
                Role = "Flight Attendant"
            };
            ViewData["FlightAttendantList"] = GetFlightAttendant(id);

            return View(fc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FlightAttendant_2(FlightCrew fc)
        {
            ViewData["FlightAttendantList"] = GetFlightAttendant(fc.ScheduleID);
            if (ModelState.IsValid)
            {
                flightcrewContext.Add(fc);
                return RedirectToAction("FlightAttendant_3", new { id = fc.ScheduleID });
            }
            else
            {
                return View(fc);
            }
        }

        public IActionResult FlightAttendant_3(int id)
        {
            // Stop accessing the action if not logged in 
            // or account not in the "Admin" role 
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            FlightCrew fc = new FlightCrew
            {
                ScheduleID = id,
                Role = "Flight Attendant"
            };
            ViewData["FlightAttendantList"] = GetFlightAttendant(id);

            return View(fc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FlightAttendant_3(FlightCrew fc)
        {
            ViewData["FlightAttendantList"] = GetFlightAttendant(fc.ScheduleID);
            if (ModelState.IsValid)
            {
                flightcrewContext.Add(fc);
                return RedirectToAction("AssignSchedule");
            }
            else
            {
                return View(fc);
            }
        }
    }
}
