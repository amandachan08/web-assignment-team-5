#pragma checksum "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4c28891b201c6891e41af4c8441753dbe212a040"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Weather), @"mvc.1.0.view", @"/Views/Home/Weather.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\_ViewImports.cshtml"
using web_assignment_team_5;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\_ViewImports.cshtml"
using web_assignment_team_5.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4c28891b201c6891e41af4c8441753dbe212a040", @"/Views/Home/Weather.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bf3bbf69566eefc93376d99b4d2b002e8215f3b5", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Weather : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<web_assignment_team_5.Models.Weather>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
  
    ViewData["Title"] = "Weather";
    Layout = "~/Views/Shared/_HomeLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h4 class=\"PageTitle\" style=\"font-weight: bold;\">Singpore Weather Forecast</h4>\r\n<label>Travelling in Singapore? This the current weather forecast!</label>\r\n\r\n");
#nullable restore
#line 11 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
 if (Model != null)
{

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <div class=""table-responsive"">
        <table id=""weather"" class=""table table-striped table-bordered"">
            <thead class=""thead-dark"">
                <tr>
                    <th>Time Stamp</th>
                    <th>Forecast</th>
                    <th>Relative Humidity (Low)</th>
                    <th>Relative Humidity (High)</th>
                    <th>Temperature (Low)</th>
                    <th>Temperature (High)</th>
                </tr>
            </thead>
            <tbody>
");
#nullable restore
#line 26 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                 for (int i = 0; i < Model.Items.Count; i++)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr>\r\n                        <td>");
#nullable restore
#line 29 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                       Write(Model.Items[i].Timestamp);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 30 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                       Write(Model.Items[i].General.Forecast);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 31 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                       Write(Model.Items[i].General.Relative_humidity.Low);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 32 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                       Write(Model.Items[i].General.Relative_humidity.High);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 33 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                       Write(Model.Items[i].General.Temperature.Low);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 34 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                       Write(Model.Items[i].General.Temperature.High);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    </tr>\r\n");
#nullable restore
#line 36 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </tbody>\r\n        </table>\r\n    </div>\r\n");
#nullable restore
#line 40 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"

}
else
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <span style=\"color:red\">No record found!</span>\r\n");
#nullable restore
#line 45 "C:\Users\sandy\Source\Repos\web-assignment-team-5\web assignment team 5\Views\Home\Weather.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<web_assignment_team_5.Models.Weather> Html { get; private set; }
    }
}
#pragma warning restore 1591
