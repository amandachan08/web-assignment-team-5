﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_team_5.Models;
using Microsoft.AspNetCore.Identity;

namespace web_assignment_team_5.DAL
{
    public class AircraftDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public AircraftDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                "AirFlightConnectionStrings");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Aircraft> GetAllAircrafts()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Aircraft ORDER BY AircraftID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into an aircraft list
            List<Aircraft> aircraftList = new List<Aircraft>();
            while (reader.Read())
            {
                aircraftList.Add(
                    new Aircraft
                    {
                        AircraftID = reader.GetInt32(0),
                        MakeModel = reader.GetString(1),
                        NumEconomySeat = reader.GetInt32(2),
                        NumBusinessSeat = reader.GetInt32(3),
                        DateLastMaintenance = reader.GetDateTime(4),
                        Status = reader.GetString(5),
                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return aircraftList;
        }

        public int Add(Aircraft aircraft)
        {
            //Create an SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an INSERT SQL statement which will
            //return the auto-generated AircraftID after insertion
            cmd.CommandText = @"INSERT INTO Aircraft (MakeModel, NumEconomySeat, NumBusinessSeat, DateLastMaintenance, Status)
                                OUTPUT INSERTED.AircraftID
                                VALUES(@makeModel, @numEconomySeat, @numBusinessSeat, @dateLastMaintenance, @status)";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class' property
            cmd.Parameters.AddWithValue("@makeModel", aircraft.MakeModel);
            cmd.Parameters.AddWithValue("@numEconomySeat", aircraft.NumEconomySeat);
            cmd.Parameters.AddWithValue("@numBusinessSeat", aircraft.NumBusinessSeat);
            cmd.Parameters.AddWithValue("@dateLastMaintenance", aircraft.DateLastMaintenance);
            cmd.Parameters.AddWithValue("@status", aircraft.Status);

            //A connection to database must be opened before any operations made
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //AircraftID after executing the INSERT SQL statement
            aircraft.AircraftID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations
            conn.Close();

            //Return id when no error occurs
            return aircraft.AircraftID;
        }

        public Aircraft GetDetails(int aircraftID)
        {
            Aircraft aircraft = new Aircraft();

            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT * FROM Aircraft
                                WHERE AircraftID = @selectedAircraftID";

            cmd.Parameters.AddWithValue("selectedAircraftID", aircraftID);

            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    aircraft.AircraftID = aircraftID;
                    aircraft.MakeModel = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    aircraft.NumEconomySeat = !reader.IsDBNull(2) ? reader.GetInt32(2) : (int?)null;
                    aircraft.NumBusinessSeat = !reader.IsDBNull(3) ? reader.GetInt32(3) : (int?)null;
                    aircraft.DateLastMaintenance = !reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?)null;
                    aircraft.Status = !reader.IsDBNull(5) ? reader.GetString(5) : null;
                }
            }

            reader.Close();

            conn.Close();
            return aircraft;
        }

        public int Update(Aircraft aircraft)
        {
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"UPDATE Aircraft SET Status=@status WHERE AircraftID = @selectedAircraftID";

            cmd.Parameters.AddWithValue("@status", aircraft.Status);
            cmd.Parameters.AddWithValue("@selectedAircraftID", aircraft.AircraftID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();
            return count;
        }
       
    }
}
