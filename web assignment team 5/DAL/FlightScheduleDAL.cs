﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.DAL
{
    public class FlightScheduleDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //Constructor
        public FlightScheduleDAL()
        {
            //Read ConnectionString from appsettings.json file 
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AirFlightConnectionStrings");

            //Instantiate a SqlConnection object with the 
            //Connection String read. 
            conn = new SqlConnection(strConn);
        }

        public List<FlightSchedule> GetAllFlightSchedule()
        {
            //SqlCommmand object from connnection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statment
            cmd.CommandText = @"SELECT * FROM FlightSchedule";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a staff list
            List<FlightSchedule> flightscheduleList = new List<FlightSchedule>();
            while (reader.Read())
            {
                flightscheduleList.Add(
                    new FlightSchedule
                    {
                        ScheduleID = reader.GetInt32(0),
                        FlightNumber = reader.GetString(1),
                        RouteID = reader.GetInt32(2),
                        AircraftID = (int)(!reader.IsDBNull(3) ? reader.GetInt32(3) : 0),
                        DepartureDateTime = reader.GetDateTime(4),
                        ArrivalDateTime = reader.GetDateTime(5),
                        EconomyClassPrice = reader.GetDecimal(6),
                        BusinessClassPrice = reader.GetDecimal(7),
                        Status = reader.GetString(8),
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return flightscheduleList;

        }

        public List<FlightSchedule> GetRouteIDFlightSchedule(int RouteID)
        {
            //SqlCommmand object from connnection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statment
            cmd.CommandText = @"SELECT * FROM FlightSchedule where RouteID = @RouteID";
            cmd.Parameters.AddWithValue("@RouteID", RouteID);

            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a staff list
            List<FlightSchedule> flightscheduleList = new List<FlightSchedule>();
            while (reader.Read())
            {
                flightscheduleList.Add(
                    new FlightSchedule
                    {
                        ScheduleID = reader.GetInt32(0),
                        FlightNumber = reader.GetString(1),
                        RouteID = reader.GetInt32(2),
                        AircraftID = (int)(!reader.IsDBNull(3) ? reader.GetInt32(3) : 0),
                        DepartureDateTime = reader.GetDateTime(4),
                        ArrivalDateTime = reader.GetDateTime(5),
                        EconomyClassPrice = reader.GetDecimal(6),
                        BusinessClassPrice = reader.GetDecimal(7),
                        Status = reader.GetString(8)
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return flightscheduleList;

        }
        public int CreateSchedule(FlightSchedule flight)
        {
            //Create an SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an INSERT SQL statement which will
            //return the auto-generated ScheduleID after insertion
            cmd.CommandText = @"INSERT INTO FlightSchedule (FlightNumber, RouteID, DepartureDateTime, ArrivalDateTime, EconomyClassPrice, BusinessClassPrice, Status)
                                OUTPUT INSERTED.ScheduleID
                                VALUES(@FlightNumber, @RouteID, @DepartureDateTime, @ArrivalDateTime, @EconomyClassPrice, @BusinessClassPrice, @Status)";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class' property
            cmd.Parameters.AddWithValue("@FlightNumber", flight.FlightNumber);
            cmd.Parameters.AddWithValue("@RouteID", flight.RouteID);
            //cmd.Parameters.AddWithValue("@AircraftID", null);
            cmd.Parameters.AddWithValue("@DepartureDateTime", flight.DepartureDateTime);
            cmd.Parameters.AddWithValue("@ArrivalDateTime", flight.ArrivalDateTime);
            cmd.Parameters.AddWithValue("@EconomyClassPrice", flight.EconomyClassPrice);
            cmd.Parameters.AddWithValue("@BusinessClassPrice", flight.BusinessClassPrice);
            cmd.Parameters.AddWithValue("@Status", flight.Status);

            //A connection to database must be opened before any operations made
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //ScheduleID after executing the INSERT SQL statement


            int a = (int)cmd.ExecuteScalar();


            //A connection should be closed after operations
            conn.Close();

            //Return id when no error occurs
            return a;
        }
        
        public FlightSchedule GetDetails(int scheduleID)
        {
            FlightSchedule flight = new FlightSchedule();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE ScheduleID = @selectedScheduleID";
            cmd.Parameters.AddWithValue("selectedScheduleID", scheduleID);

            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    flight.ScheduleID = scheduleID;
                    flight.FlightNumber = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    flight.RouteID = (int)(!reader.IsDBNull(2) ? reader.GetInt32(2) : (int?)null);
                    flight.AircraftID = (int)(!reader.IsDBNull(3) ? reader.GetInt32(3) : 0);
                    flight.DepartureDateTime = (DateTime)(!reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?)null);
                    flight.ArrivalDateTime = (DateTime)(!reader.IsDBNull(5) ? reader.GetDateTime(5) : (DateTime?)null);
                    flight.EconomyClassPrice = (decimal)(!reader.IsDBNull(6) ? reader.GetDecimal(6) : (decimal?)null);
                    flight.BusinessClassPrice = (decimal)(!reader.IsDBNull(7) ? reader.GetDecimal(7) : (decimal?)null);
                    flight.Status = (!reader.IsDBNull(8) ? reader.GetString(8) : null);
                }
            }
            reader.Close();
            conn.Close();
            return flight;
        }
        
        public int Update(FlightSchedule flight)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"Update FlightSchedule SET Status=@status WHERE ScheduleID = @selectedScheduleID";

            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from respective class's property. 
            cmd.Parameters.AddWithValue("@status", flight.Status);

            cmd.Parameters.AddWithValue("@selectedScheduleID", flight.ScheduleID);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();  
            //Executes a Transact-SQL statement against the connection and returns the number of rows
            
            //Close the database connection
            conn.Close();

            return count;
        }

        public int UpdateAircraft(FlightSchedule flightSchedule)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE FlightSchedule SET AircraftID=@aircraftID WHERE ScheduleID = @selectedScheduleID";

            if (flightSchedule.AircraftID != 0)
                cmd.Parameters.AddWithValue("@aircraftID", flightSchedule.AircraftID);
            else
                cmd.Parameters.AddWithValue("@aircraftID", DBNull.Value);

            cmd.Parameters.AddWithValue("@selectedScheduleID", flightSchedule.ScheduleID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

 
    }
}
