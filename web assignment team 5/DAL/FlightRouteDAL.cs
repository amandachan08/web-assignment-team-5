﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.DAL
{
    public class FlightRouteDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        public FlightRouteDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                "AirFlightConnectionStrings");

            //Instantiate a SqlConnection object
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<FlightRoute> GetAllFlightRoutes()
        {
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM FlightRoute ORDER BY RouteID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            List<FlightRoute> flightRouteList = new List<FlightRoute>();

            while (reader.Read())
            {
                flightRouteList.Add(
                    new FlightRoute
                    {
                        RouteID = reader.GetInt32(0),
                        DepartureFlightCity = reader.GetString(1),
                        DepartureFlightCountry = reader.GetString(2),
                        ArrivalFlightCity = reader.GetString(3),
                        ArrivalFlightCountry = reader.GetString(4),
                        FlightDuration = reader.GetInt32(5),
                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return flightRouteList;
        }

        //TODO: Create flight route
        public int CreateRoute(FlightRoute flight)
        {
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"INSERT INTO FlightRoute (DepartureCity, DepartureCountry, ArrivalCity, ArrivalCountry, FlightDuration)
                                OUTPUT INSERTED.RouteID
                                VALUES (@DepartCity, @DepartCountry, @ArrivalCity, @ArrivalCountry, @Duration)";
            cmd.Parameters.AddWithValue("@DepartCity", flight.DepartureFlightCity);
            cmd.Parameters.AddWithValue("@DepartCountry", flight.DepartureFlightCountry);
            cmd.Parameters.AddWithValue("@ArrivalCity", flight.ArrivalFlightCity);
            cmd.Parameters.AddWithValue("@ArrivalCountry", flight.ArrivalFlightCountry);
            cmd.Parameters.AddWithValue("@Duration", flight.FlightDuration);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            int a = (int)cmd.ExecuteScalar();
            
            conn.Close();

            return a;
        }

       
    }
}