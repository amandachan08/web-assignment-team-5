﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.DAL
{
    public class FlightCrewDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //Constructor
        public FlightCrewDAL()
        {
            //Read ConnectionString from appsettings.json file 
            var builder = new ConfigurationBuilder() 
                .SetBasePath(Directory.GetCurrentDirectory()) 
                .AddJsonFile("appsettings.json"); 
            
            Configuration = builder.Build(); 
            string strConn = Configuration.GetConnectionString("AirFlightConnectionStrings"); 
            
            //Instantiate a SqlConnection object with the 
            //Connection String read. 
            conn = new SqlConnection(strConn);
        }

        public List<FlightCrew> GetAllFlightCrew()
        {
            //SqlCommmand object from connnection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statment
            cmd.CommandText = @"SELECT * FROM FlightCrew ORDER BY StaffID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a staff list
            List<FlightCrew> flightcrewList = new List<FlightCrew>();
            while (reader.Read())
            {
                flightcrewList.Add(
                    new FlightCrew
                    {
                        ScheduleID = reader.GetInt32(0),
                        StaffID = reader.GetInt32(1),
                        Role = reader.GetString(2),
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return flightcrewList;
        }

        public int Add(FlightCrew fc)
        {
            //Create a SqlCommand object from connection object 
            SqlCommand cmd = conn.CreateCommand();

            //Specify an INSERT SQL statement which will 
            //return the auto-generated StaffID after insertion 
            cmd.CommandText = @"INSERT INTO FlightCrew (ScheduleID, StaffID, Role) VALUES(@scheduleId, @staffId, @role)";

            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from respective class's property. 
            cmd.Parameters.AddWithValue("@scheduleId", fc.ScheduleID);
            cmd.Parameters.AddWithValue("@staffId", fc.StaffID);
            cmd.Parameters.AddWithValue("@role", fc.Role);

            //A connection to database must be opened before any operations made. 
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated 
            //StaffID after executing the INSERT SQL statement 
            cmd.ExecuteScalar();

            //A connection should be closed after operations. 
            conn.Close();

            //Return id when no error occurs. 
            return fc.ScheduleID;

        }

        public int Delete(int scheduleID)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            // to delete a staff record specified by a Schedule ID
            
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM FlightCrew WHERE ScheduleID = @selectedScheduleID";
            cmd.Parameters.AddWithValue("@selectedScheduleID", scheduleID);

            //Open a database connection
            conn.Open();

            int rowAffected = 0;

            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();

            //Close database connection
            conn.Close();

            //Return number of row of staff record updated or deleted
            return rowAffected;
        }
    }
}
