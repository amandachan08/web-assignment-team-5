﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.DAL
{
    public class CustomerDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        public CustomerDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "AirFlightConnectionStrings");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public int Add(CustReg customer)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Customer (CustomerName, Nationality, BirthDate, TelNo,
                                EmailAddr)
                                OUTPUT INSERTED.CustomerID
                                VALUES(@CustomerName, @Nationality, @BirthDate, @TelNo,
                                @EmailAddr)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@CustomerName", customer.CustomerName);
            cmd.Parameters.AddWithValue("@Nationality", customer.Nationality);
            cmd.Parameters.AddWithValue("@BirthDate", customer.CustDOB);
            cmd.Parameters.AddWithValue("@TelNo", customer.TelNo);
            cmd.Parameters.AddWithValue("@EmailAddr", customer.CustEmail);


            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            customer.CustomerID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return customer.CustomerID;
        }
        public int Update(CustReg customerID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"UPDATE CustReg SET Password=@Password
                                WHERE CustomerID = @selectedCustomerID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@Password", customerID.Password);
            //Open a database connection
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        }

        public int? isUserExist(string LoginID, string Password)
        { 
            int? customerID = null;
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"Select CustomerID from Customer
                                        where EmailAddr = @SelectedEmailAddr and Password = @SelectedPassword";
            cmd.Parameters.AddWithValue("@SelectedEmailAddr", LoginID);
            cmd.Parameters.AddWithValue("@SelectedPassword", Password);
            conn.Open();
            SqlDataReader reader =cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
               {
                    customerID = reader.GetInt32(0);
               }
            }
            reader.Close();
            conn.Close();

            return customerID;
        
        }

    }
}
