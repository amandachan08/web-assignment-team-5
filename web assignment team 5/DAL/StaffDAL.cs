﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_team_5.Models;

namespace web_assignment_team_5.DAL
{
    public class StaffDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor 
        public StaffDAL() 
        { 
            //Read ConnectionString from appsettings.json file 
            var builder = new ConfigurationBuilder() 
                .SetBasePath(Directory.GetCurrentDirectory()) 
                .AddJsonFile("appsettings.json"); 

            Configuration = builder.Build(); 
            string strConn = Configuration.GetConnectionString("AirFlightConnectionStrings"); 
            
            //Instantiate a SqlConnection object with the 
            //Connection String read. 
            conn = new SqlConnection(strConn); 
        }

        public List<Staff> GetAllStaff()
        {
            //SqlCommmand object from connnection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statment
            cmd.CommandText = @"SELECT * FROM Staff ORDER BY StaffID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a staff list
            List<Staff> staffList = new List<Staff>();
            while (reader.Read())
            {
                staffList.Add(
                    new Staff
                    {
                        StaffID = reader.GetInt32(0),
                        StaffName = reader.GetString(1),
                        //Get the first character of a string
                        Gender = reader.GetString(2)[0],
                        DateEmployed = reader.GetDateTime(3),
                        Vocation = reader.GetString(4),
                        EmailAddr = reader.GetString(5),
                        Password = reader.GetString(6),
                        Status = reader.GetString(7),
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return staffList;

        }

        public int Add(Staff staff)
        {
            //Create a SqlCommand object from connection object 
            SqlCommand cmd = conn.CreateCommand();

            //Specify an INSERT SQL statement which will 
            //return the auto-generated StaffID after insertion 
            cmd.CommandText = @"INSERT INTO Staff (StaffName, Gender, DateEmployed, Vocation, EmailAddr) OUTPUT INSERTED.StaffID VALUES(@name, @gender, @dateemployed, @vocation, @email)";

            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from respective class's property. 
            cmd.Parameters.AddWithValue("@name", staff.StaffName); 
            cmd.Parameters.AddWithValue("@gender", staff.Gender); 
            cmd.Parameters.AddWithValue("@dateemployed", staff.DateEmployed); 
            cmd.Parameters.AddWithValue("@vocation", staff.Vocation); 
            cmd.Parameters.AddWithValue("@email", staff.EmailAddr);

            staff.Password = "p@55Staff";
            staff.Status = "Active";

            //A connection to database must be opened before any operations made. 
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated 
            //StaffID after executing the INSERT SQL statement 
            staff.StaffID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations. 
            conn.Close(); 
            
            //Return id when no error occurs. 
            return staff.StaffID;

        }

        public bool IsEmailExist(string email, int staffId)
        {
            bool emailFound = false;

            //Create a SqlCommand object and specify the SQL statement
            //to get a staff record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT StaffID FROM Staff WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read()){
                    if (reader.GetInt32(0) != staffId)
                        emailFound = true;
                }
            }
            else
            {
                emailFound = false;
            }

            reader.Close();
            conn.Close();

            return emailFound;

        }

        public Staff GetDetails(int staffId)
        {
            Staff staff = new Staff();

            // Create a SqlCommand onject from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statment
            cmd.CommandText = @"SELECT * FROM Staff WHERE StaffID = @selectedStaffID";

            //Define the parameter used in SQL statement, value for the 
            //parameter is retrieved from the method parameter “staffId”. 
            cmd.Parameters.AddWithValue("@selectedStaffID", staffId);

            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader
                    staff.StaffID = staffId;
                    staff.StaffName = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    staff.Gender = !reader.IsDBNull(2) ? reader.GetString(2)[0] : (char)0;
                    staff.DateEmployed = (DateTime)(!reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null);
                    staff.Vocation = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    staff.EmailAddr = !reader.IsDBNull(5) ? reader.GetString(5) : null;
                    staff.Password = !reader.IsDBNull(6) ? reader.GetString(6) : null;
                    staff.Status = !reader.IsDBNull(7) ? reader.GetString(7) : null;
                }
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return staff;
        }

        // Return number of row updated 
        public int Update(Staff staff)
        {
            //Create a SqlCommand object from connection object 
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement 
            cmd.CommandText = @"UPDATE Staff SET Status=@status WHERE StaffID = @selectedStaffID";

            //Define the parameters used in SQL statement, value for each parameter 
            //is retrieved from respective class's property. 
            cmd.Parameters.AddWithValue("@status", staff.Status);

            cmd.Parameters.AddWithValue("@selectedStaffID", staff.StaffID);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();  //Executes a Transact-SQL statement against the connection and returns the number of rows
            //Close the database connection
            conn.Close();

            return count;
        }
    }
}
