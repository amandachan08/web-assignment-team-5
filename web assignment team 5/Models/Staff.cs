﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class Staff
    {
        [Display(Name ="Staff ID")]
        public int StaffID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters. ")]
        public string StaffName { get; set; }

        public char Gender { get; set; }

        [Display(Name = "Date of Employment")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Date Employed is required.")]
        public DateTime DateEmployed { get; set; }

        [Display(Name = "Vocation")]
        [Required(ErrorMessage = "Vocation is required.")]
        public string Vocation { get; set; }

        [Display(Name = "Email Address")]
        [EmailAddress]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        [StringLength(50, ErrorMessage = "Email address cannot exceed 50 characters. ")]
        [Required(ErrorMessage = "Email Address is required.")]
        public string EmailAddr { get; set; }

        public string Password { get; set; }

        public string Status { get; set; }

    }
}
