﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class Aircraft
    {
        [Display(Name = "Aircraft ID")]
        public int AircraftID { get; set; }

        [Display(Name = "Aircraft Model")]
        [Required(ErrorMessage = "Please enter the Aircraft Model.")]
        public string MakeModel { get; set; }

        [Display(Name = "Number of Economy Seats")]
        [Required(ErrorMessage = "Please enter the number of Economy Seats.")]
        public int? NumEconomySeat { get; set; }

        [Display(Name = "Number of Business Seats")]
        [Required(ErrorMessage = "Please enter the number of Business Seats.")]
        public int? NumBusinessSeat { get; set; }

        [Display(Name = "Date of Last Maintenance")]
        [DataType(DataType.Date)]
        public DateTime? DateLastMaintenance { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; } = "Operational";
    }
}
