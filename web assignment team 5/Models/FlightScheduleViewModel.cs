﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class FlightScheduleViewModel
    {
        [Display(Name = "Schedule ID")] 
        public int ScheduleID { get; set; }

        public string FlightNumber { get; set; }

        [Display(Name = "Aircraft ID")]
        public int AircraftID { get; set; }

        
        [Display(Name = "Departure Date Time")] 
        [DataType(DataType.DateTime)] 
        public DateTime? DepartureDateTime { get; set; }

        [Display(Name = "Arrival Date Time")] 
        [DataType(DataType.DateTime)] 
        public DateTime? ArrivalDateTime { get; set; }

        //advanced parts. 
        

        [Display(Name = "Economy Class Price")]
        public float EconomyClassPrice{ get; set; }

        [Display(Name = "Business Class Price")]
        public float BusinessClassPrice { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}
