﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class FlightRoute
    {

        [Display(Name = "Route ID")]
        [Required(ErrorMessage = "Route ID is required.")]
        public int RouteID { get; set; }

        [Display(Name = "Departure City")]
        [Required(ErrorMessage = "Departure City is required.")]
        public string DepartureFlightCity { get; set; }

        [Display(Name = "Departure Country")]
        [Required(ErrorMessage = "Departure Country is required.")]
        public string DepartureFlightCountry { get; set; }

        [Display(Name = "Arrival City")]
        [Required(ErrorMessage = "Arrival City is required.")]
        public string ArrivalFlightCity { get; set; }

        [Display(Name = "Arrival Country")]
        [Required(ErrorMessage = " Arrival Country is required.")]
        public string ArrivalFlightCountry { get; set; }

        [Display(Name = "Flight Duration")]
        [Required(ErrorMessage = "Flight Duration is required.")]
        [Range(1, int.MaxValue)]
        public int FlightDuration { get; set; }

        public List<FlightSchedule> flightSchedules { get; set; }
    }

}
