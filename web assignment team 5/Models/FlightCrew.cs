﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class FlightCrew
    {
        [Display(Name = "Schedule ID")]
        public int ScheduleID { get; set; }

        [Display(Name = "Staff ID")]
        [Required(ErrorMessage = "Email Address is required.")]
        public int StaffID { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }
    }
}
