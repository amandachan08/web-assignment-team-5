﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class StaffViewModel
    {
        [Display(Name = "ID")]
        public int StaffID { get; set; }

        [Display(Name = "Name")]
        public string StaffName { get; set; }

        [Display(Name = "Gender")]
        public char Gender { get; set; }

        [Display(Name = "Date of Employment")]
        public DateTime DateEmployed { get; set; }

        [Display(Name = "Vocation")]
        public string Vocation { get; set; }

        [Display(Name = "Email Address")]
        public string EmailAddr { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        public List<int> ScheduleNo { get; set; }

        public List<string> Role { get; set; }

        public List<DateTime> DepartureDateTime { get; set; }

    }
}
