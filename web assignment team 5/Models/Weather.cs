﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ValidPeriod
    {
        [JsonProperty("start")]
        public DateTime Start { get; set; }
        [JsonProperty("end")]
        public DateTime End { get; set; }
    }

    public class RelativeHumidity
    {
        [JsonProperty("low")]
        public int Low { get; set; }
        [JsonProperty("high")]
        public int High { get; set; }
    }

    public class Temperature
    {
        [JsonProperty("low")]
        public int Low { get; set; }
        [JsonProperty("high")]
        public int High { get; set; }
    }

    public class Speed
    {
        [JsonProperty("low")]
        public int Low { get; set; }
        [JsonProperty("high")]
        public int High { get; set; }
    }

    public class Wind
    {
        [JsonProperty("speed")]
        public Speed Speed { get; set; }
        [JsonProperty("direction")]
        public string Direction { get; set; }
    }

    public class General
    {
        [JsonProperty("forecast")]
        public string Forecast { get; set; }
        [JsonProperty("relative_humidity")]
        public RelativeHumidity Relative_humidity { get; set; }
        [JsonProperty("temperature")]
        public Temperature Temperature { get; set; }
        [JsonProperty("wind")]
        public Wind Wind { get; set; }
    }

    public class Time
    {
        [JsonProperty("start")]
        public DateTime Start { get; set; }
        [JsonProperty("end")]
        public DateTime End { get; set; }
    }

    public class Regions
    {
        [JsonProperty("west")]
        public string West { get; set; }
        [JsonProperty("east")]
        public string East { get; set; }
        [JsonProperty("central")]
        public string Central { get; set; }
        [JsonProperty("south")]
        public string South { get; set; }
        [JsonProperty("north")]
        public string North { get; set; }
    }

    public class Period
    {
        [JsonProperty("time")]
        public Time Time { get; set; }
        [JsonProperty("regions")]
        public Regions Regions { get; set; }
    }

    public class Item
    {
        [JsonProperty("update_timestamp")]
        public DateTime Update_timestamp { get; set; }
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
        [JsonProperty("valid_period")]
        public ValidPeriod Valid_period { get; set; }
        [JsonProperty("general")]
        public General General { get; set; }
        [JsonProperty("periods")]
        public List<Period> Periods { get; set; }
    }

    public class ApiInfo
    {
        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class Weather
    {
        [JsonProperty("items")]
        public List<Item> Items { get; set; }
        [JsonProperty("api_info")]
        public ApiInfo Api_info { get; set; }
    }
}
