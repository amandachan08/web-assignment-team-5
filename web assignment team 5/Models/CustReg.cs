﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_assignment_team_5.Models
{
    public class CustReg
    {

        [Display(Name = "CustomerID")]
        public int CustomerID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Your Name")]

        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Please Enter Your Nationality")]
        [Display(Name = "Nationality")]
        public string Nationality { get; set; }


        [Required(ErrorMessage = "Please Enter Your Date of Birth")]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime CustDOB { get; set; }


        [Required(ErrorMessage = "Please Enter Your Telephone Number")]
        [Display(Name = "Telephone Number")]
        public string TelNo { get; set; }

        [Required(ErrorMessage = "Please Enter Your Email Address")]
        [Display(Name = "Email Address")]
        [EmailAddress]
        public string CustEmail { get; set; }

        [Required(ErrorMessage = "Please enter your Password")]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
