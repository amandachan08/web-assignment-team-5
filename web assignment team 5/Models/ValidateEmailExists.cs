﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using web_assignment_team_5.DAL;

namespace web_assignment_team_5.Models
{
    public class ValidateEmailExists : ValidationAttribute
    {
        private StaffDAL staffConext = new StaffDAL();

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Get email value to validate 
            string email = Convert.ToString(value);
            //Casting the validation context to the "Staff" model claass
            Staff staff = (Staff)validationContext.ObjectInstance;
            //Get the Staff Id from the staff instance 
            int staffId = staff.StaffID;

            if (staffConext.IsEmailExist(email, staffId))
                // validation failed
                return new ValidationResult("Email address already exists!");
           
            else
                // validation passed
                return ValidationResult.Success;
        }
    }
}
