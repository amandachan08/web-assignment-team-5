﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_assignment_team_5.Models
{
    public class FlightRouteViewModel
    {
        [Display(Name = "Route ID")]
        public int RouteID { get; set; }

        [Display(Name = "Departure City")]
        public string DepartureFlightCity { get; set; }

        [Display(Name = "Departure Country")]
        public string DepartureFlightCountry { get; set; }

        [Display(Name = "Arrival City")]
        public string ArrivalFlightCity { get; set; }

        [Display(Name = "Arrival Country")]
        public string ArrivalFlightCountry { get; set; }

        [Display(Name = "Flight Duration")]
        public int FlightDuration { get; set; }

    }
}
