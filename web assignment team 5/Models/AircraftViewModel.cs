﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class AircraftViewModel
    {
        [Display(Name = "ID")]
        public int AircraftID { get; set; }

        [Display(Name = "Aircraft Model")]
        public string MakeModel { get; set; }

        [Display(Name = "Departure DateTime")]
        public List<DateTime> DepartureDateTime { get; set; }

        [Display(Name = "Arrival DateTime")]
        public List<DateTime> ArrivalDateTime { get; set; }

        [Display(Name = "Departure Country")]
        public List<string> DepartureCountry { get; set; }

        [Display(Name = "Arrival Country")]
        public List<string> ArrivalCountry { get; set; }

    }
}
