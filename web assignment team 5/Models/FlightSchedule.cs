﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace web_assignment_team_5.Models
{
    public class FlightSchedule
    {
        [Display(Name = "Schedule ID")]
        public int ScheduleID { get; set; }
        [Display(Name = "Flight Identification Number")]
        [Required(ErrorMessage = "Flight Identification number is required.")]
        public string FlightNumber { get; set; }
        [Display(Name = "Route Identification Number")]
        [Required(ErrorMessage = "Route Identification is required.")]
        public int RouteID { get; set; }
        [Display(Name = "Aircraft ID")]
        public int? AircraftID { get; set; }
        [Display(Name = "Departure date and time")]
        [Required(ErrorMessage = "Departure date and time is required.")]
        public DateTime DepartureDateTime { get; set; }
        [Display(Name = "Arrival date and time")]
        [Required(ErrorMessage = "Arrival date and time is required.")]
        public DateTime ArrivalDateTime { get; set; }
        [Display(Name = "Economy Seat Price")]
        [Required(ErrorMessage = "Economy Seat price is required.")]
        public decimal EconomyClassPrice { get; set; }
        [Display(Name = "Business Seat Price")]
        [Required(ErrorMessage = "Business Seat Price and time is required.")]
        public decimal BusinessClassPrice { get; set; }
        [Display(Name = "Status of Flight")]
        [Required(ErrorMessage = "Flight's Status is required.")]
        public string Status { get; set; }
    }
}
